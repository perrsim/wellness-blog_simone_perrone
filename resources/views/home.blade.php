@extends('layouts.app')

@section('content')

<header>
    <div class="overlay"></div>
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="/video/video.mp4" type="video/mp4">
        </video>
        <div class="container h-100">
            <div class="d-flex h-100 text-center align-items-center">
                <div class="w-100 text-white">
                    <h1 class="display-3">Welcome to <br> WELLNESS BLOG</h1>
                </div>
            </div>
        </div>
    </header>
    
    {{-- CARDS --}}
    <div class="container-fluid my-5  py-5 bg-custom-1">
        <div class="row d-flex justify-content-center">
            <div class="col-12 col-md-8">
                <h1 class="text-center text-newsletter text-color-custom">BLOG</h1>
                <p class="text-center">Le ultime dal blog. Un punto di riferimento per chi vuole essere aggiornato sul mondo della salute, dello sport, delle ricette più gustose ma equilibrate e sul benessere a tutto tondo.</b></p>
            </div>
        </div>
        
        <div class="row justify-content-center my-2">
            
            <div class="card mt-5" style="width: 16rem;">
                <img src="/img/alimentazione.png" class="card-img-top img-fluid" alt="...">
                <div class="card-body">
                    <h5 class="text-center"><b>Alimentazione</b></h5>
                    <p class="card-text text-center">Ricette, informazioni nutrizionali, curiosità sul cibo, con particolare focus sul bilanciamento con i prodotti Pensa Benessere.</p>
                </div>
            </div>
            
            <div class="card mt-5 ml-3" style="width: 16rem;">
                <img src="/img/healt.png" class="card-img-top img-fluid" alt="...">
                <div class="card-body">
                    <h5 class="text-center"><b>Salute</b></h5>
                    <p class="card-text text-center">Raffreddore ai primi freddi? Allergia in primavera? Come sconfiggerli con l'aiuto dei prodotti Pensa Benessere, info e curiosità.</p>
                </div>
            </div>
            
            <div class="card mt-5 ml-3" style="width: 16rem;">
                <img src="/img/sport.png" class="card-img-top img-fluid" alt="...">
                <div class="card-body">
                    <h5 class="text-center"><b>Sport</b></h5>
                    <p class="card-text text-center">Sport conosciuti e meno conosciuti, sportivi famosi e curiosità sulla loro alimentazione, gli esercizi da integrare per mantenere uno stile di vita sano.</p>
                </div>
            </div>
            
            <div class="card mt-5 ml-3" style="width: 16rem;">
                <img src="/img/news.png" class="card-img-top img-fluid" alt="...">
                <div class="card-body">
                    <h5 class="text-center"><b>News</b></h5>
                    <p class="card-text text-center">Tutte le news legate al benessere, allo sport e agli integratori Pensa Benessere.</p>
                </div>
            </div>
            
        </div>
    </div>
    
    
    
    
    
    <div class="container mt-3">
        <div class="row align-items-center ">
            <div class="col-12 col-md-6 mt-3">
                <img class="img-fluid img-aboutus mt-0" src="/img/about.png" alt="aboutus">
            </div>
            <div class="col-12 col-md-6 mt-0">
                <h1 class="font-weight-bold text-newsletter">WELLNESS BLOG</h1>
                <p>Linea d’integratori alimentari di qualità 100% made in Italia del gruppo Pensa Pharma. <br>
                    Dal Febbraio 2020 è entrata a far parte della multinazionale Giapponese Towa Pharmaceuticals che che si dedica alla ricerca,
                    sviluppo, produzione e commercializzazione di farmaci generici dal 1951.
                    Pensa Pharma sviluppa i suoi prodotti con un approccio sostenibile e responsabile,
                    pensando al benessere e alla sicurezza del paziente che lo assumerà.
                    Scegliere Pensa significa scegliere un percorso di valore equivalente.
                </p>
            </div>
        </div>
    </div>
    
    
    <div class="container mb-3">
        <div class="row align-items-center mt-0 ">
            <div class="col-12 col-md-6 mt-0">
                <h1>Pensa a Colori</h1>
                
                <p>Pensa a colori è un’iniziativa di Pensa Benessere. Abbiamo chiesto ai nostri follower di pensare a ciò che li fa stare bene e trasmette energia ogni giorno e scattare una fotografia che esprimesse benessere, ottimismo, positività.
                    Pensa a colori ha consentito a tutti di condividere un pensiero che parlasse del loro ottimismo e positività usando l’hashtog #pensachebello e pubblicando la foto sui loro canali social.
                    Pensa Benessere è colori!</p>
                </div>
                <div class="col-12 col-md-6 mt-0">
                    <img class="img-fluid mt-0 rounded-circle" src="/img/notes.png" alt="aboutus">
                </div>
            </div>
        </div>
        
        
        
        
        @endsection
        