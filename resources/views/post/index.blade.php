@extends('layouts.app')

@section('content')

<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-12 col-md-9">
            @if (session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
            
            @endif
            <h1 class="text-orange">I miei articoli</h1>

            <div class="row">
            @foreach ($posts as $post)
                <div class="card mr-3" style="width: 16rem;">
                    <img src=" {{ Storage::url($post->img) }} " class="card-img-top img-fluid" alt="">
                    <div class="card-body">
                        <h5 class="card-title text-truncate"> {{ $post->title}} </h5>
                        <p class="card-text text-truncate"> {{ $post->slug}} </p>
                        <a href="{{ route('post.show', $post) }}" class="btn btn-custom">Vai all'articolo</a>
                    </div>
                </div>
            @endforeach
        </div>
            
        </div>
        
        <div class="col-12 col-md-3">
            <h3 class="mb-3 text-orange">link veloci:</h3>
            <div class="card" style="width: 18rem;">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><a href=" {{route('post.create')}} ">Crea Articolo</a></li>
                    <li class="list-group-item"><a href=" {{route('post.admin')}} ">Gestisci articoli</a></li>
                </ul>
            </div>
        </div>
        
    </div>
</div>




@endsection