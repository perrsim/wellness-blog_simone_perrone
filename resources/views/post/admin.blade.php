@extends('layouts.app')

@section('content')

    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-12">
                @if (session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
            
            @endif
                <h1 class="text-orange">Gestisci i tuoi articoli</h1>
            </div>
        </div>
    </div>

    <div class="container mb-5">
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">TITOLO</th>
                        <th scope="col">SOTTOTITOLO</th>
                        <th scope="col">AZIONI</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($posts as $post)
                        <tr>
                            <th scope="row"> {{ $post->id }} </th>
                            <td> {{ $post->title }} </td>
                            <td> {{ $post->slug}} </td>
                            <td class="d-flex">
                                <a href=" {{ route('post.edit', $post) }} " class="btn btn-warning mr-2">Modifica</a>

                                <form action="{{ route('post.destroy', $post)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Cancella</button>
                                </form>

                            </td>
                          </tr>
                          <tr>
                        @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
    
@endsection