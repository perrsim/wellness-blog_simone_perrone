@extends('layouts.app')

@section('content')


<div class="container mt-5 mb-5">
    <div class="row d-flex justify-content-center">
        <div class="col-12 col-md-8">
            <h1 class="text-orange text-center">Crea un nuovo articolo</h1>
            
            <form enctype="multipart/form-data" method="POST" action="{{route('post.store')}}">
                @csrf
                <div class="form-group">
                    <label for="title">Titolo</label>
                    <input type="text" class="form-control" name="title">
                </div>
                <div class="form-group">
                    <label for="slug">Sottotitolo</label>
                    <input type="text" class="form-control" name="slug">
                </div>
                <div class="form-group">
                    <label for="img"> Immagine</label>
                    <input type="file" name="img" class="form-group">
                </div>
                <div class="form-group">
                    <label for="body">Corpo del post</label>
                    <textarea name="body" cols="30" rows="10" class="form-control"></textarea>
                </div>
                <button type="submit" class="btn btn-custom">Inserisci</button>
            </form>
            
        </div>
    </div>
</div>


@endsection