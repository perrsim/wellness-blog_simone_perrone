@extends('layouts.app')


@section('content')

<div class="container mt-5 mb-5 py-3 px-3">
    <div class="row d-flex justify-content-center">
        <div class="col-12 col-md-8 mx-2 my-2">
            <div class="">
                <img src=" {{ Storage::url($post->img) }} " class=" img-fluid" alt="">
                <div class="">
                    <h2 class="mt-4 mb-4"> {{ $post->title}} </h2>
                    <p class=""> {{ $post->slug}} </p>
                    <p> {{$post->body}} </p>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection