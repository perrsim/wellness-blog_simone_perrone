@extends('layouts.app')

@section('content')

@if (session('message'))
    <div class="alert alert-success">
        {{ session('status') }}    
    </div>    
@endif


<div class="container">
    <div class="row text-center">
        <div class="col-12">
            <h1 class="text-orange">Modifica il tuo post</h1>
        </div>
    </div>
</div>


<div class="container mt-3 mb-5">
    <div class="row d-flex justify-content-center">
        <div class="col-12 col-md-8">
            
            <form method="POST" action="{{route('post.update', $post )}}">
                @csrf
                <div class="form-group">
                    <label for="title">Titolo</label>
                    <input type="text" class="form-control" name="title" value="{{ $post->title}}">
                </div>
                <div class="form-group">
                    <label for="slug">Sottotitolo</label>
                    <input type="text" class="form-control" name="slug" value=" {{ $post->slug}} ">
                </div>
                <div class="form-group">
                    <label for="body">Corpo del post</label>
                    <textarea name="body" cols="30" rows="10" class="form-control"> {{ $post->body}} </textarea>
                </div>
                <button type="submit" class="btn btn-custom">Conferma modifica</button>
            </form>
            
        </div>
    </div>
</div>


@endsection