<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ config('app.name', 'Laravel') }}</title>
    
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    {{-- navbar --}}
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" alt=""> 
                <img src="/img/wellness.png" class=" logo img-fluid">
            </a>
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href=" {{ route('home')}} ">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Chi Siamo</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=" {{route('contattaci')}} ">Contattaci</a>
                    </li>
                </ul>
            </div>
            <span class="float-right">
                <button type="button" class="btn btn-custom mr-3">
                    <a href="{{route('post.index')}}"> Profilo, ADMIN</a>
                </button> 
            </span>
        </nav>
        
        
        @yield('content')
        
        {{-- footer --}}
        <footer class="background-footer text-lg-start mt-5">
            <!-- Grid container -->
            <div class="container p-4">
                <!--Grid row-->
                <div class="row">
                    <!--Grid column-->
                    <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
                        <img src="/img/wellness.png" class="logo">
                        <h5 class="text-uppercase">Il concetto di benessere</h5>
                        
                        <p>
                            Il termine "wellness" nasce come l'insieme di due termini "well being" e "fitness". Il suo vero significato rimane tuttavia un mistero fra i maggiormente insoluti nel campo del Benessere.
                        </p>
                    </div>
                    <!--Grid column-->
                    
                    <!--Grid column-->
                    <div class="col-lg-3 col-md-6 mb-4 mb-md-0 text-center">
                        <h5 class="text-uppercase mt-3">Siti amici</h5>
                        
                        <ul class="list-unstyled mb-0">
                            <li>
                                <a href="#!" class="text-dark">Benesere Blog</a>
                            </li>
                            <li>
                                <a href="#!" class="text-dark">Pensa Benessere</a>
                            </li>
                            <li>
                                <a href="#!" class="text-dark">IO benessere</a>
                            </li>
                            <li>
                                <a href="#!" class="text-dark">Farmacia igea</a>
                            </li>
                        </ul>
                    </div>
                    <!--Grid column-->
                    
                    <!--Grid column-->
                    <div class="col-lg-3 col-md-6 mb-4 mb-md-0 text-center">
                        <img src="/img/benessere1.png" class="img-fluid img-footer">
                    </div>
                    <!--Grid column-->
                </div>
                <!--Grid row-->
            </div>
            <!-- Grid container -->
            
            <!-- Copyright -->
            <div class="text-center p-3 custom-background">
                © 2020 Copyright:
                <a class="text-dark" href="https://mdbootstrap.com/">Wellness.com</a>
            </div>
            <!-- Copyright -->
        </footer>
        <!-- Footer -->
    </body>
    </html>
    