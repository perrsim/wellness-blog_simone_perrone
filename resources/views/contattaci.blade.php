@extends('layouts.app')

@section('content')

<div class="container mt-5 mb-5">
    <div class="row justify-content-center">
        <div class="col-8">

            <div class="col-8">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li> {{ $error }} </li>
                        @endforeach
                    </ul>
                </div>
                    
                @endif
            </div>
            
            <form method="POST" action=" {{route('contact.send')}} ">
                @csrf
                <div class="form-group">
                    <label for="name">Nome e Cognome</label>
                    <input type="text" class="form-control" name="name" value=" {{ old('name') }} ">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="mail" value=" {{ old('email') }} ">
                </div>
                <div class="form-group">
                    <label for="text">Inserisci testo</label>
                    <textarea type="text" class="form-control" name="messaggio">
                        {{ old('messaggio')}}
                    </textarea>
                </div>
                <button type="submit" class="btn btn-custom">Invia</button>
            </form>
            
        </div>
    </div>
</div>





@endsection