<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ContactController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/create', [PostController::class, 'create'])->name('post.create');

Route::post('/post/store', [PostController::class, 'store'])->name('post.store');

Route::get('/post/index', [PostController::class, 'index'])->name('post.index');

Route::get('/post/show/{post}', [PostController::class, 'show'])->name('post.show');

Route::get('/admin', [PostController::class, 'admin'])->name('post.admin');

Route::get('/post/edit/{post}', [PostController::class, 'edit'])->name('post.edit');

Route::post('/post/{post}/update/', [PostController::class, 'update'])->name('post.update');

Route::delete('/post/{post}/delete', [PostController::class, 'destroy'])->name('post.destroy');

Route::get('/contact', [HomeController::class, 'contattaci'])->name('contattaci');

Route::post('/contact', [ContactController::class, 'contact'])->name('contact.send');

Route::get('/thx', [ContactController::class,'thx'])->name('mail.thx');